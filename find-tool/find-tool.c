/* main.c
 *
 * Copyright 2001 Ximian Inc.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <glade/glade.h>
#include <gnome.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <signal.h>
#include <ctype.h>

#define GLADE_FILE "ximian-find.glade"

GladeXML *gui;
typedef enum {
	FIND_SEARCH,
	LOCATE_SEARCH
} SearchMode;


typedef enum {
	ARG_TYPE_STRING,
	ARG_TYPE_BOOLEAN
} ArgType;

typedef enum {
	ARG_NORMAL,
	ARG_NOT,
	ARG_SHALLOW,
} ArgFlags;

typedef struct 
{
	char *title;
	ArgType type;
	char *arg;
	ArgFlags flags;
} Arg;

typedef struct _Line
{
	struct _Line *prev;
	struct _Line *next;
	int num;
	Arg *arg;
	GtkWidget *widget;
	GtkWidget *combo;
	GtkWidget *remove;
} Line;

Line *first_line = NULL;
Line *last_line = NULL;

GtkWidget *first_remove_btn = NULL;

static Arg args[] = {
	{
		N_("File name is"),
		ARG_TYPE_STRING,
		"-iname",
		ARG_NORMAL
	},
	{
		N_("File name is not"), 
		ARG_TYPE_STRING,
		"-iname",
		ARG_NOT
	},
	{
		N_("Don't search mounted filesystems"),
		ARG_TYPE_BOOLEAN,
		"-mount",
		ARG_NORMAL
	},
	{
		N_("Follow symbolic links"),
		ARG_TYPE_BOOLEAN,
		"-follow",
		ARG_NORMAL
	},
	{
		N_("Process directory contents depth first"),
		ARG_TYPE_BOOLEAN,
		"-depth",
		ARG_NORMAL
	},
	{
		N_("Don't search subdirectories"),
		ARG_TYPE_BOOLEAN,
		NULL,
		ARG_SHALLOW
	},
	{
		N_("Owned by group"),
		ARG_TYPE_STRING,
		"-group",
		ARG_NORMAL
	},
	{
		N_("Owned by user"),
		ARG_TYPE_STRING,
		"-user",
		ARG_NORMAL
	},
	{
		N_("Matches regular expression"),
		ARG_TYPE_STRING,
		"-regex",
		ARG_NORMAL
	},
	{
		NULL
	}
};

SearchMode search_mode;

int table_size = 0;

pid_t child_pid = -1;

static void exit_cb                 (GtkWidget      *widget,
				     gpointer        data);
static void add_cb                  (GtkWidget      *widget,
				     gpointer        data);
static void search_cb               (GtkWidget      *widget,
				     gpointer        data);
static void cancel_cb               (GtkWidget      *widget,
				     gpointer        data);
static int  results_button_press_cb (GtkWidget      *widget,
				     GdkEventButton *event,
				     gpointer        data);

gboolean find_finished = 0;


static int
check_find_finished_cb ()
{
	GtkWidget *w;
	if (find_finished) {
		child_pid = -1;
		w = glade_xml_get_widget (gui, "search_btn");
		gtk_widget_set_sensitive (GTK_WIDGET (w), TRUE);
		w = glade_xml_get_widget (gui, "cancel_btn");
		gtk_widget_set_sensitive (GTK_WIDGET (w), FALSE);
		find_finished = 0;
	}	
	return TRUE;
}


static void
sig_cld ()
{
	pid_t pid;
	int status;

	if (signal (SIGCLD, sig_cld) == (void*)-1) 
		g_error ("signal() error");
	if ((pid = wait (&status)) < 0)
		g_error ("wait() error");

	find_finished = 1;
}

static GtkWidget *
string_widget_create_func (Arg *arg)
{
	return gtk_entry_new ();
}

static GtkWidget *
boolean_widget_create_func (Arg *arg)
{
	GtkWidget *ret = gtk_check_button_new_with_label ("Enabled");
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (ret), 1);
	return ret;
}

typedef GtkWidget *(*WidgetCreateFunc) (Arg *arg);
WidgetCreateFunc create_funcs[] = {
	string_widget_create_func, boolean_widget_create_func
};

static void
select_arg (Line *line, Arg *arg)
{
	GtkWidget *table = glade_xml_get_widget (gui, "selection_table");

	if (line->widget) {
		gtk_container_remove (GTK_CONTAINER (table), line->widget);
		line->widget = NULL;
	}

	line->arg = arg;

	line->widget = create_funcs[arg->type](arg);
	gtk_table_attach (GTK_TABLE (table), line->widget, 1, 3, 
			  line->num, line->num + 1, GTK_EXPAND | GTK_FILL, 0, 2, 2);
	gtk_widget_show_all (line->widget);
}


static void
arg_selected (GtkWidget *w, Arg *arg)
{
	Line *line = gtk_object_get_data (GTK_OBJECT (w), "line");
	select_arg (line, arg);
}

static void
create_selector (Line *line)
{
	Arg *i;
	GtkWidget *ret = gtk_option_menu_new ();
	GtkWidget *menu = gtk_menu_new ();

	for (i = args; i->title != NULL; i++) {
		GtkWidget *item;
		item = gtk_menu_item_new_with_label (_(i->title));
		gtk_object_set_data (GTK_OBJECT (item), "line", 
				     line);
		gtk_signal_connect (GTK_OBJECT (item), "activate",
				    GTK_SIGNAL_FUNC (arg_selected), 
				    i);
		gtk_menu_append (GTK_MENU (menu), item);
	}
	
	gtk_widget_show_all (menu);
	gtk_option_menu_set_menu (GTK_OPTION_MENU (ret), menu);

	line->combo = ret;
}

static void
move_line (GtkWidget *table, Line *line, int offset)
{
	gtk_object_ref (GTK_OBJECT (line->combo));
	gtk_object_ref (GTK_OBJECT (line->widget));
	gtk_object_ref (GTK_OBJECT (line->remove));

	gtk_container_remove (GTK_CONTAINER (table), line->combo);
	gtk_container_remove (GTK_CONTAINER (table), line->widget);
	gtk_container_remove (GTK_CONTAINER (table), line->remove);

	line->num--;
	
	gtk_table_attach (GTK_TABLE (table), line->combo, 0, 1, 
			  line->num, line->num + 1, GTK_FILL, 0, 2, 2);
	gtk_table_attach (GTK_TABLE (table), line->widget, 1, 3, 
			  line->num, line->num + 1, GTK_EXPAND | GTK_FILL, 0, 2, 2);
	gtk_table_attach (GTK_TABLE (table), line->remove, 3, 4,
			  line->num, line->num + 1, 0, 0, 2, 2);

	gtk_object_unref (GTK_OBJECT (line->combo));
	gtk_object_unref (GTK_OBJECT (line->widget));
	gtk_object_unref (GTK_OBJECT (line->remove));
}

static void
remove_cb (GtkWidget *w, gpointer data)
{
	Line *line = (Line*)data;
	Line *i;
	
	GtkWidget *table = glade_xml_get_widget (gui, "selection_table");
	
	gtk_container_remove (GTK_CONTAINER (table), line->combo);
	gtk_container_remove (GTK_CONTAINER (table), line->widget);
	gtk_container_remove (GTK_CONTAINER (table), line->remove);
	
	/* Remove the line from the list */
	if (line->prev)	line->prev->next = line->next;
	if (line->next) line->next->prev = line->prev;
	if (first_line == line) first_line = line->next;
	if (last_line == line) last_line = line->prev;

	for (i = line->next; i != NULL; i = i->next) {
		move_line (table, i, -1);
	}
	
	if (--table_size == 1) {
		gtk_widget_set_sensitive (GTK_WIDGET (first_remove_btn),
					  FALSE);
	}
}

static void
add_line ()
{
	
	Line *line = g_new0 (Line, 1);
	GtkWidget *table = glade_xml_get_widget (gui, "selection_table");
	GtkWidget *pixmap;
	
	create_selector (line);

	gtk_widget_show (line->combo);

	gtk_table_resize (GTK_TABLE (table), ++table_size, 4);
	gtk_table_attach (GTK_TABLE (table), line->combo, 0, 1, 
			  table_size - 1, table_size, GTK_FILL, 0, 2, 2);
	line->num = table_size - 1;

	if (last_line) last_line->next = line;
	line->prev = last_line;
	last_line = line;
	if (!first_line) first_line = line;
	
	select_arg (line, &args[0]);

	pixmap = gnome_stock_pixmap_widget_new (GTK_WIDGET (table),
						GNOME_STOCK_PIXMAP_REMOVE);
	
	line->remove = gnome_pixmap_button (pixmap, "Remove");
	gtk_signal_connect (GTK_OBJECT (line->remove), "clicked",
			    GTK_SIGNAL_FUNC (remove_cb), line);
	if (table_size == 1) {
		gtk_widget_set_sensitive (GTK_WIDGET (line->remove),
					  FALSE);
		first_remove_btn = line->remove;
	} else {
		gtk_widget_set_sensitive (GTK_WIDGET (first_remove_btn), TRUE);
	}
	
	gtk_widget_show (line->remove);
	gtk_table_attach (GTK_TABLE (table), line->remove, 3, 4,
			  table_size - 1, table_size, 0, 0, 2, 2);
}

void
add_cb (GtkWidget *widget, gpointer data)
{
	add_line ();
}

void
exit_cb (GtkWidget *widget, gpointer data)
{
	gtk_main_quit ();
}

#define BUF_SIZE 1024

static gboolean
search_input_cb (GIOChannel *chan, GIOCondition cond, gpointer user_data)
{
	GtkWidget *clist = user_data;

	char terminator = (search_mode == LOCATE_SEARCH) ? '\n' : '\0';

	char in_buf[BUF_SIZE];
	static char *fragment = NULL;
	
	int read;

	g_io_channel_read (chan, in_buf, BUF_SIZE - 1, &read);
	if (read > 0) {
		char *last_begin = in_buf;
		char *p;
		
		for (p = in_buf; (p - in_buf) < read; p++) {
			if (*p == terminator) {
				gchar *text[1];
				*p = '\0';
				if (fragment) {
					text[0] = 
						g_strdup_printf ("%s%s", 
								 fragment,
								 last_begin);
					
					gtk_clist_append (GTK_CLIST (clist), 
							  text);
					g_free (text[0]);
					g_free (fragment);
					fragment = NULL;
				} else {
					text[0] = last_begin;
					gtk_clist_append (GTK_CLIST (clist), 
							  text);
				}						
				last_begin = p + 1;
			}		
		}
		if (last_begin != p) {
			*p = 0;
			fragment = g_strdup_printf ("%s%s",
						    fragment ? fragment : "",
						    last_begin);
		}
			
	}
	return TRUE;
}

static void
destroy_command (GArray *cmd)
{
	char **p = (char**)cmd->data;
	
	while (*p != 0) {
		g_free (*p);
		p++;
	}

	g_array_free (cmd, TRUE);
}

static gboolean
check_empty (char *str)
{
	gboolean ret = TRUE;
	char *p;

	for (p = str; *p != 0; p++) {
		if (!isspace (*p)) {
			ret = FALSE;
			break;
		}
	}
	return ret;
}


#define BUILD_CMD_ERROR_RETURN(m) {GtkWidget *dlg = gnome_error_dialog (m); gnome_dialog_run_and_close (GNOME_DIALOG (dlg)); destroy_command (ret); return NULL;}

static GArray *
build_locate_command ()
{
	GArray *ret = g_array_new (TRUE, TRUE, sizeof (char *));	
	GtkWidget *w;
	char *p;
	
	p = g_strdup ("locate");
	g_array_append_val (ret, p);
	
	w = glade_xml_get_widget (gui, "filename_entry");
	
	p = g_strdup (gtk_editable_get_chars (GTK_EDITABLE (w),
					      0, -1));
	if (check_empty (p)) {
		BUILD_CMD_ERROR_RETURN (_("No search string specified"));
	}
	g_array_append_val (ret, p);

	return ret;
}

static GArray *
build_find_command ()
{
	GArray *ret = g_array_new (TRUE, TRUE, sizeof (char *));
	GtkWidget *w;
	int i;
	Line *line;
	char *tmp;
	char *tmp2;
	
	w = glade_xml_get_widget (gui, "start_directory");
	
	tmp = g_strdup ("find");
	g_array_append_val (ret, tmp);
	
	tmp = gtk_editable_get_chars (GTK_EDITABLE (w), 0, -1);
	if (!g_file_test (tmp, G_FILE_TEST_ISDIR)) {
		g_free (tmp);
		BUILD_CMD_ERROR_RETURN (_("Could not find the specified directory."));
	}	
	
	g_array_append_val (ret, tmp);
	for (i = 1, line = first_line; line != NULL; i++, line = line->next) {
		Arg *arg = line->arg;
		
		if (arg->flags == ARG_NOT) {
			tmp = g_strdup ("!");
			g_array_append_val (ret, tmp);
		}
		
		switch (arg->type) {
		case ARG_TYPE_STRING :
			tmp = g_strdup (gtk_editable_get_chars (GTK_EDITABLE (line->widget), 0, -1));
			if (check_empty (tmp)) {
				g_free (tmp);
				BUILD_CMD_ERROR_RETURN (_("Argument not specified"));
			}
			
			tmp2 = g_strdup_printf (arg->arg);
			g_array_append_val (ret, tmp2);
			g_array_append_val (ret, tmp);
			
			break;
		case ARG_TYPE_BOOLEAN :
			if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (line->widget))) {
				if (arg->flags == ARG_SHALLOW) {
					tmp = g_strdup ("-maxdepth");
					g_array_append_val (ret, tmp);
					tmp = g_strdup ("1");
					g_array_append_val (ret, tmp);
				} else {
					tmp = g_strdup (arg->arg);
					
					g_array_append_val (ret, tmp);
				}
			}
			break;
			default :
				break;
		}
	}
	tmp = g_strdup ("-print0");		
	g_array_append_val (ret, tmp);

	return ret;
}

static GArray *
build_command ()
{
	if (search_mode == LOCATE_SEARCH) {
		return build_locate_command ();
	} else {
		return build_find_command ();
	}
}

void
cancel_cb (GtkWidget *widget, gpointer data)
{
	g_return_if_fail (child_pid != -1);

	kill (child_pid, SIGKILL);
}

void
search_cb (GtkWidget *widget, gpointer data) 
{
	GIOChannel *chan;
	GtkWidget *w;
	GArray *cmd;

	int fd[2];
	int page;

	g_return_if_fail (child_pid == -1);

	w = glade_xml_get_widget (gui, "notebook1");
	page = gtk_notebook_get_current_page (GTK_NOTEBOOK (w));

	search_mode = (page == 0) ? LOCATE_SEARCH : FIND_SEARCH;

	cmd = build_command ();
	if (cmd == NULL) {
		return;
	}
	
	w = glade_xml_get_widget (gui, "search_btn");
	gtk_widget_set_sensitive (GTK_WIDGET (w), FALSE);
	w = glade_xml_get_widget (gui, "cancel_btn");
	gtk_widget_set_sensitive (GTK_WIDGET (w), TRUE);

	pipe (fd);
	child_pid = fork ();
	if (child_pid < 0) {
		g_error ("Unable to fork.");
	} else if (child_pid) {
		GtkWidget *clist;
		/* Parent */
		close (fd[1]);
		chan = g_io_channel_unix_new (fd[0]);
		if (search_mode == LOCATE_SEARCH) {
			clist = glade_xml_get_widget (gui, "results_1");
			g_assert (clist);
		} else {
			clist = glade_xml_get_widget (gui, "results_2");
			g_assert (clist);
		}
		
		gtk_clist_clear (GTK_CLIST (clist));

		g_io_add_watch (chan, G_IO_IN, search_input_cb, clist);
		destroy_command (cmd);
	} else {
		w = glade_xml_get_widget (gui, "filename_entry");
		
		/* Child */
		close (fd[0]);
		dup2 (fd[1], STDOUT_FILENO);
		execvp (((char**)cmd->data)[0], (char**)cmd->data);
		g_error ("unable to exec %s", ((char**)cmd->data)[0]);
		exit (1);
	}
}

int
results_button_press_cb (GtkWidget *widget, 
			 GdkEventButton *event,
			 gpointer data)
{
	if (event->type == GDK_2BUTTON_PRESS) {
		int row, column;
		if (gtk_clist_get_selection_info (GTK_CLIST (widget),
						  event->x, event->y,
						  &row, &column)) {
			char *text;
			if (gtk_clist_get_text (GTK_CLIST (widget), 
						row, column, &text)) {
				char *argv[] = {
					"ximian-file-launcher",
					NULL,
					NULL 
				};
				
				argv[1] = text;
				
				gnome_execute_async (g_get_current_dir (), 2, argv);
			}
		}
	}
	return 1;
}

static void
connect_signals ()
{
	GtkWidget *w;
	
	w = glade_xml_get_widget (gui, "search_dialog");
	gtk_signal_connect (GTK_OBJECT (w),
			    "close", GTK_SIGNAL_FUNC (exit_cb), NULL);
	w = glade_xml_get_widget (gui, "search_btn");
	gtk_signal_connect (GTK_OBJECT (w),
			    "clicked", GTK_SIGNAL_FUNC (search_cb), NULL);
	w = glade_xml_get_widget (gui, "cancel_btn");
	gtk_signal_connect (GTK_OBJECT (w),
			    "clicked", GTK_SIGNAL_FUNC (cancel_cb), NULL);
	w = glade_xml_get_widget (gui, "close_btn");
	gtk_signal_connect (GTK_OBJECT (w),
			    "clicked", GTK_SIGNAL_FUNC (exit_cb), NULL);
	w = glade_xml_get_widget (gui, "filename_entry");
	gtk_signal_connect (GTK_OBJECT (w),
			    "activate", GTK_SIGNAL_FUNC (search_cb), NULL);
	w = glade_xml_get_widget (gui, "results_1");
	gtk_signal_connect (GTK_OBJECT (w),
			    "button_press_event", 
			    GTK_SIGNAL_FUNC (results_button_press_cb), NULL);
	w = glade_xml_get_widget (gui, "results_2");
	gtk_signal_connect (GTK_OBJECT (w),
			    "button_press_event", 
			    GTK_SIGNAL_FUNC (results_button_press_cb), NULL);
	w = glade_xml_get_widget (gui, "button8");
	gtk_signal_connect (GTK_OBJECT (w),
			    "clicked", GTK_SIGNAL_FUNC (add_cb), NULL);
}

static void
check_programs ()
{
	gboolean has_locate = TRUE;
	char *path;
	
	if (!(path = gnome_is_program_in_path ("locate"))) {
		GtkWidget *w = glade_xml_get_widget (gui, "notebook1");
		gtk_notebook_set_show_tabs (GTK_NOTEBOOK (w), FALSE);
		gtk_notebook_set_page (GTK_NOTEBOOK (w), 1);
		has_locate = FALSE;
	}
	g_free (path);
	
	if (!(path = gnome_is_program_in_path ("find"))) {
		if (has_locate) {
			GtkWidget *w = glade_xml_get_widget (gui, "notebook1");
			gtk_notebook_set_show_tabs (GTK_NOTEBOOK (w), FALSE);
			gtk_notebook_set_page (GTK_NOTEBOOK (w), 0);
		} else {
			GtkWidget *dlg = gnome_error_dialog (_("Could not find either locate or find on your system.  The find utility cannot run without either locate or find."));
			gnome_dialog_run_and_close (GNOME_DIALOG (dlg));
			exit (1);
		}			
	}
}

int 
main(int argc, char* argv[])
{
	GtkWidget *w;
	
	bindtextdomain(PACKAGE, GNOMELOCALEDIR);  
	textdomain(PACKAGE);

	gnome_init (PACKAGE, VERSION, argc, argv);
	glade_gnome_init ();

	if (signal (SIGCLD, sig_cld) == (void*)-1)
		g_error ("signal() error");

	gui = glade_xml_new (GLADEDIR "/" GLADE_FILE, NULL);
	
	if (!gui) {
		g_error ("Could not find " GLADE_FILE);
	}

	connect_signals ();
	check_programs ();

	gtk_timeout_add (250, check_find_finished_cb, NULL);

	add_line ();

	w = glade_xml_get_widget (gui, "start_directory");
	gtk_entry_set_text (GTK_ENTRY (w), g_get_home_dir ());

	w = glade_xml_get_widget (gui, "search_dialog");
	gtk_widget_show (w);

	gtk_main();

	if (child_pid != -1)
		cancel_cb (NULL, NULL);
	
	return 0;
}
