/* launcher.c
 *
 * Copyright 2001 Ximian Inc.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <gnome.h>
#include <bonobo.h>

#include <libgnomevfs/gnome-vfs-mime.h>
#include <libgnomevfs/gnome-vfs-mime-info.h>
#include <libgnomevfs/gnome-vfs-mime-handlers.h>

static gboolean components_available (const char *mime_type);



static gboolean 
view_file_with_application (char *filename)
{
	const char *mime_type = gnome_vfs_get_file_mime_type (filename, 
							      NULL, FALSE);
	GnomeVFSMimeApplication *app;

	app = gnome_vfs_mime_get_default_application (mime_type);

	if (app) {
		char **argv;

		if (app->requires_terminal) {
			argv = g_new0 (char *, 4);
			argv[0] = "gnome-terminal";
			argv[1] = "--command";
			argv[2] = g_strdup_printf ("%s %s", 
						   app->command, filename);
			
		} else {
			argv = g_new0 (char*, 3);
			argv[0] = app->command;
			argv[1] = filename;
		}

		execvp (argv[0], argv);

		/* exec failed */
		if (app->requires_terminal) {
			g_free (argv[2]);
		}

		g_free (argv);
		gnome_vfs_mime_application_free (app);
	}

	return FALSE;
}

static gboolean 
components_available (const char *mime_type)
{
	CORBA_Environment ev;
	OAF_ServerInfoList *oaf_result;
	CORBA_char *query;
	gboolean ret = FALSE;
	char *generic;
	char *p;
	
	generic = g_strdup (mime_type);
	p = strchr (generic, '/');
	g_assert (p);
	*(++p) = '*';
	*(++p) = 0;
	
	CORBA_exception_init (&ev);
	query = g_strdup_printf ("repo_ids.has ('IDL:Bonobo/Control:1.0') AND (bonobo:supported_mime_types.has ('%s') OR bonobo:supported_mime_types.has ('%s')) AND (repo_ids.has ('IDL:Bonobo/PersistFile:1.0') OR repo_ids.has ('IDL:Bonobo/PersistStream:1.0'))", mime_type, generic);
	
	oaf_result = oaf_query (query, NULL, &ev);

	g_free (generic);	       
	g_free (query);
	
	if (ev._major == CORBA_NO_EXCEPTION && oaf_result != NULL && oaf_result->_length >= 1) {
		ret = TRUE;
	}
	
	if (oaf_result != NULL) {
		CORBA_free (oaf_result);
	}
	CORBA_exception_free (&ev);

	return ret;
}

static void
launch_viewer (char *filename)
{
	char **argv = g_new0 (char*, 3);
	argv[0] = "ximian-file-viewer";
	argv[1] = filename;
	
	execvp (argv[0], argv);
}

static void
launch_nautilus (char *filename)
{
	char **argv = g_new0 (char*, 3);
	argv[0] = "nautilus";
	argv[1] = filename;

	execvp (argv[0], argv);
}

static gboolean 
nautilus_is_running () 
{
	CORBA_Environment ev;
	CORBA_Object obj;
	gboolean ret;

	CORBA_exception_init (&ev);
	obj = oaf_activate_from_id ("OAFIID:nautilus_factory:bd1e1862-92d7-4391-963e-37583f0daef3",
				    OAF_FLAG_EXISTING_ONLY, NULL, &ev);
	
	ret = !CORBA_Object_is_nil (obj, &ev);	

	CORBA_Object_release (obj, &ev);
	
	CORBA_exception_free (&ev);
	
	return ret;
}

static void
no_viewer_message (char *file)
{
	char *msg = g_strdup_printf (_("Could not find a viewer for %s."), file);
	GtkWidget *dlg = gnome_error_dialog (msg);
	gnome_dialog_run_and_close (GNOME_DIALOG (dlg));
}

int 
main(int argc, char* argv[])
{
	char *file;
	
	bindtextdomain(PACKAGE, GNOMELOCALEDIR);  
	textdomain(PACKAGE);

	gnome_init_with_popt_table ("ximian-file-launcher", VERSION,
				    argc, argv, oaf_popt_options, 0, NULL);
	oaf_init (argc, argv);
	if (!bonobo_init (oaf_orb_get (), NULL, NULL))
		g_error (_("Can't initialize bonobo"));
	
	bonobo_activate ();
	
	file = argv[1];
	
	if (nautilus_is_running ()) {
		launch_nautilus (file);
	}

	if (!g_file_exists (file)) {
		char *msg = g_strdup_printf (_("Could not find %s"), file);
		GtkWidget *dlg = gnome_error_dialog (msg);
		gnome_dialog_run_and_close (GNOME_DIALOG (dlg));
		return 1;
	}

	view_file_with_application (file);
	
	if (components_available (file)) 
		launch_viewer (file);
	
	/* If we've gotten here, there is no viewer available */
	no_viewer_message (file);

	return 2;
}
