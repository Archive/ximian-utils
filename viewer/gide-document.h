/* gIDE
 * Copyright (C) 1998-2000 Steffen Kern
 *               2000-2001 Dave Camp <dave@ximian.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef GIDE_DOCUMENT_H
#define GIDE_DOCUMENT_H

#include <config.h>
#include <bonobo.h>

#define GIDE_DOCUMENT(o)          (GTK_CHECK_CAST ((o), gide_document_get_type(), GideDocument))
#define GIDE_DOCUMENT_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), gide_document_get_type(), GideDocumentClass))
#define GIDE_IS_DOCUMENT(o)       (GTK_CHECK_TYPE ((o), gide_document_get_type()))

typedef struct _GideDocument            GideDocument;
typedef struct _GideDocumentClass       GideDocumentClass;
typedef struct _GideDocumentPrivate     GideDocumentPrivate;

struct _GideDocumentClass {
	GtkVBoxClass vbox;
	
	void (* modified) (GideDocument *document);
	void (* unmodified) (GideDocument *document);
        void (* changed) (GideDocument *document, gint change_type);
	void (* readonly) (GideDocument *document);
	void (* unreadonly) (GideDocument *document);
	void (* source) (GideDocument *document, gchar *filename);
	void (* cursor) (GideDocument* document);	void (* focus) (GideDocument* document);
};

struct _GideDocument {
	GtkVBox parent;
	
	Bonobo_UIContainer ui_container;
	BonoboControlFrame *control_frame;
	GtkWidget *bonobo_widget;
	Bonobo_PersistFile persist_file;
	Bonobo_PersistStream persist_stream;
	
	gboolean file_loaded;
	char *filename;
	char *mime_type;
	
	GideDocumentPrivate *priv;
};

/*
 * Prototypes for 'gide_document.c'
 */
/* creation */
guint gide_document_get_type(void);
GtkWidget* gide_document_new(Bonobo_UIContainer ui_container);

/* file access */
void gide_document_make_temp (GideDocument *document, const gchar *mime_type);
void gide_document_load_file(GideDocument* document, const gchar* filename);
void gide_document_save_file(GideDocument* document, const gchar* filename);

/* Functions to get/set state */
gboolean gide_document_is_free(GideDocument* document);
gboolean gide_document_is_changed(GideDocument* document);
void gide_document_set_changed_state(GideDocument* document, gboolean state);
gboolean gide_document_is_readonly(GideDocument* document);
void gide_document_set_readonly_state(GideDocument* document, gboolean state);
gboolean gide_document_is_busy(GideDocument* document);
void gide_document_set_busy_state(GideDocument* document, gboolean state);
gchar* gide_document_get_filename(GideDocument* document);
const gchar *gide_document_get_mime_type (GideDocument *document);
void gide_document_set_last_mod(GideDocument* document, gint last_mod);

void gide_document_check_changed(GideDocument* document);
void gide_document_set_cfg_values (GideDocument *document);

BonoboObjectClient *gide_document_get_control (GideDocument *document);

#endif
