/* viewer.c
 *
 * Copyright 2001 Ximian Inc.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <gnome.h>
#include <bonobo.h>
#include "gide-document.h"

#include <libgnomevfs/gnome-vfs-mime.h>
#include <libgnomevfs/gnome-vfs-mime-info.h>
#include <libgnomevfs/gnome-vfs-mime-handlers.h>

typedef struct 
{
	GtkWidget *doc;
	GtkWidget *win;
	char *filename;
	GtkWidget *dlg;
} Viewer;

static void exit_cb (GtkWidget *w, gpointer data, char *cname);
static void open_cb (GtkWidget *w, gpointer data, char *cname);
static void save_cb (GtkWidget *w, gpointer data, char *cname);
static void save_as_cb (GtkWidget *w, gpointer data, char *cname);

static gboolean components_available (const char *mime_type);

BonoboUIVerb verbs[] = {
	BONOBO_UI_UNSAFE_VERB ("FileExit", exit_cb),
	BONOBO_UI_UNSAFE_VERB ("FileOpen", open_cb),
	BONOBO_UI_UNSAFE_VERB ("FileSave", save_cb),
	BONOBO_UI_UNSAFE_VERB ("FileSaveAs", save_as_cb),
	BONOBO_UI_VERB_END
};


static void 
exit_cb (GtkWidget *w, gpointer data, char *cname)
{
	Viewer *v = (Viewer*)data;
	
	gtk_widget_destroy (GTK_WIDGET (v->win));
	g_free (v->filename);
	g_free (v);
	
	gtk_main_quit ();
}

static void
delete_event_cb (GtkWidget *w, GdkEvent *event, gpointer data)
{
	exit_cb (w, data, "FileExit");
}

static void
file_open_cb (GtkWidget *w, gpointer data)
{
	Viewer *v = (Viewer*)data;
	char *filename;

	filename = g_strdup (gtk_file_selection_get_filename (GTK_FILE_SELECTION (v->dlg)));
	
	if (!components_available (gnome_vfs_get_file_mime_type (filename,
								 NULL,
								 FALSE))) {
		GtkWidget *dlg;
		char *msg = g_strdup_printf (_("Could not find any viewers for %s."), filename);
		
		dlg = gnome_error_dialog (msg);
		gnome_dialog_run_and_close (GNOME_DIALOG (dlg));
	} else {
		g_free (v->filename);
		v->filename = g_strdup (filename);
		gtk_window_set_title (GTK_WINDOW (v->win), v->filename);
		gide_document_load_file (GIDE_DOCUMENT (v->doc), v->filename);
	}

	gtk_widget_destroy (v->dlg);
}

static void 
open_cb (GtkWidget *w, gpointer data, char *cname)
{
	Viewer *v = (Viewer*)data;

	v->dlg = gtk_file_selection_new (_("Open File"));
	gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (v->dlg)->ok_button),
			    "clicked", GTK_SIGNAL_FUNC (file_open_cb),
			    (gpointer)v);
				      
	gtk_signal_connect_object (GTK_OBJECT (GTK_FILE_SELECTION (v->dlg)->cancel_button),
				   "clicked", GTK_SIGNAL_FUNC (gtk_widget_destroy),
				   (gpointer)v->dlg);
	gtk_widget_show (v->dlg);
}

static void
save_cb (GtkWidget *w, gpointer data, char *cname)
{
	Viewer *v = (Viewer *)data;
	gide_document_save_file (GIDE_DOCUMENT (v->doc), v->filename);
}

static void
save_as_cb (GtkWidget *w, gpointer data, char *cname)
{
	Viewer *v = (Viewer *)data;
}

static gboolean 
components_available (const char *mime_type)
{
	CORBA_Environment ev;
	OAF_ServerInfoList *oaf_result;
	CORBA_char *query;
	gboolean ret = FALSE;
	char *generic;
	char *p;

	generic = g_strdup (mime_type);
	p = strchr (generic, '/');
	g_assert (p);
	*(++p) = '*';
	*(++p) = 0;

	CORBA_exception_init (&ev);
	query = g_strdup_printf ("repo_ids.has ('IDL:Bonobo/Control:1.0') AND (bonobo:supported_mime_types.has ('%s') OR bonobo:supported_mime_types.has ('%s')) AND (repo_ids.has ('IDL:Bonobo/PersistFile:1.0') OR repo_ids.has ('IDL:Bonobo/PersistStream:1.0'))", mime_type, generic);
	
	oaf_result = oaf_query (query, NULL, &ev);

	g_free (generic);	       
	g_free (query);
	
	if (ev._major == CORBA_NO_EXCEPTION && oaf_result != NULL && oaf_result->_length >= 1) {
		ret = TRUE;
	}
	
	if (oaf_result != NULL) {
		CORBA_free (oaf_result);
	}
	CORBA_exception_free (&ev);

	return ret;
}

static gboolean
view_file_with_components (const char *filename)
{
	Viewer *v;
	BonoboUIContainer *container;
	BonoboUIComponent *component;
	Bonobo_UIContainer corba_container;

	if (!components_available (gnome_vfs_get_file_mime_type (filename,
								 NULL,
								 FALSE))) {
		return FALSE;
	}

	v = g_new0 (Viewer, 1);
	
	v->win = bonobo_window_new ("gnome-file-view", filename);
	container = bonobo_ui_container_new ();
	corba_container = BONOBO_OBJREF (container);
	bonobo_ui_container_set_win (container, BONOBO_WINDOW (v->win));

	v->doc = gide_document_new (corba_container);
	component = bonobo_ui_component_new ("ximian-file-viewer");
	bonobo_ui_component_set_container (component, corba_container);
	bonobo_ui_util_set_ui (component, DATADIR, "ximian-file-viewer.xml",
			       "ximian-file-viewer");
	bonobo_ui_component_add_verb_list_with_data (component, verbs, v);

	gide_document_load_file (GIDE_DOCUMENT (v->doc), filename);

	bonobo_window_set_contents (BONOBO_WINDOW (v->win), v->doc);
	
	gtk_window_set_default_size (GTK_WINDOW (v->win), 500, 500);

	gtk_widget_show_all (v->win);

	gtk_signal_connect (GTK_OBJECT (v->win), "delete_event",
			    GTK_SIGNAL_FUNC (delete_event_cb), v);
	v->filename = g_strdup (filename);

	bonobo_main();

	return TRUE;
}


int 
main(int argc, char* argv[])
{
	gboolean success = TRUE;
	char *file;
	
	bindtextdomain(PACKAGE, GNOMELOCALEDIR);  
	textdomain(PACKAGE);

	gnome_init_with_popt_table ("ximian-file-viewer", VERSION, argc, argv,
				    oaf_popt_options, 0, NULL);
	oaf_init (argc, argv);
	if (!bonobo_init (oaf_orb_get (), NULL, NULL))
		g_error (_("Can't initialize bonobo"));


	bonobo_activate ();

	file = argv[1];

	if (!g_file_exists (file)) {
		char *msg = g_strdup_printf ("Could not find %s", file);
		GtkWidget *dlg = gnome_error_dialog (msg);
		gnome_dialog_run_and_close (GNOME_DIALOG (dlg));
		return 1;
	}

	if (!view_file_with_components (file)) {
		success = FALSE;
	}
	
	if (!success) {
		char *msg = g_strdup_printf (_("Could not find any viewers for %s."), file);
		GtkWidget *dlg = gnome_error_dialog (msg);
		gnome_dialog_run_and_close (GNOME_DIALOG (dlg));
		return 2;
	}
	
	return 0;
}
